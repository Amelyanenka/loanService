package com.loanservice.services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.loanservice.controller.ApplicationConfig;
import com.loanservice.dao.LoanDao;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.ErrorCode;
import com.loanservice.domain.LoanTypeEntity;
import com.loanservice.domain.RequestEntity;


public class LoanServiceImplTest {
    private static SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");

    @DataProvider(name = "testCreateLoanDataProvider")
    private Object[][] testCreateLoanDataProvider()
        throws ParseException {
        ClientEntity client = new ClientEntity();
        client.setId(-1);

        List<RequestEntity> existingRequests = Arrays.asList(
                new RequestEntity[] {
                    new RequestEntity(), new RequestEntity(), new RequestEntity()
                });
        List<LoanTypeEntity> loans = Arrays.asList(new LoanTypeEntity());

        return new Object[][] {
                {
                    3, 50000d, client, "100.100.100.100", new ArrayList<RequestEntity>(), loans,
                    ErrorCode.NIGHT_REQUEST_WITH_MAX_AMOUNT, f.parse("01:30:00")
                },
                {
                    55, 50000d, client, "100.100.100.100", new ArrayList<RequestEntity>(), loans, null, f.parse("07:30:00")
                },
                {
                    55, 50000d, client, "100.100.100.100", existingRequests, loans, ErrorCode.OVER_COUNT, f.parse("07:30:00")
                },
                {
                    55, 10000d, client, "100.100.100.100", new ArrayList<RequestEntity>(), new ArrayList<LoanTypeEntity>(),
                    ErrorCode.NO_LOANS, f.parse("07:30:00")
                }
            };
    }

    @Test(dataProvider = "testCreateLoanDataProvider")
    public void testCreateLoan(int term, double amount, ClientEntity client, String ip, List<RequestEntity> requests,
        List<LoanTypeEntity> loans, ErrorCode errorCode, Date actionDate) {
        LoanServiceImpl loanService = new LoanServiceImpl();

        LoanDao dataDao = spy(LoanDao.class);
        when(dataDao.getClientByName(any(String.class), any(String.class))).thenReturn(client);

        loanService.setDataDao(dataDao);
        when(dataDao.getIpRequest(any(String.class), any(Date.class))).thenReturn(requests);

        when(dataDao.getLoanTypeList(any(Integer.class), any(Double.class), any(Date.class))).thenReturn(loans);

        when(dataDao.getRequestList(client.getId(), ip, true)).thenReturn(requests);

        ApplicationConfig config = spy(ApplicationConfig.class);
        loanService.setApplicationConfig(config);
        when(config.getMaxAmount()).thenReturn(50000d);
        when(config.getMaxAttemptCount()).thenReturn(3);
        when(config.getStartRiskPeriodString()).thenReturn("00:00:00");
        when(config.getEndRiskPeriodString()).thenReturn("06:00:00");
        when(config.getNow()).thenReturn(actionDate);

        RequestEntity requestEntity = loanService.createLoan(term, amount, client.getId(), ip);

        Assert.assertEquals(errorCode, requestEntity.getErrorCode());

        Mockito.verify(dataDao, times(1)).saveEntity(any(RequestEntity.class), any(Class.class));
    }
} // end class LoanServiceImplTest
