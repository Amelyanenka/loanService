package com.loanservice.pattern;

public interface Callable<V, E extends Throwable> {
    V call()
        throws E;
}
