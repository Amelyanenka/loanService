package com.loanservice.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.LoanTypeEntity;
import com.loanservice.domain.RequestEntity;
import com.loanservice.domain.Status;


@Repository(LoanDao.BEAN_NAME)
public class LoanDaoImpl
    implements LoanDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<RequestEntity> getRequestList(final Integer clientId, final String remoteAddr, boolean initLazy) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(RequestEntity.class);

        if (clientId != null) {
            criteria.add(Restrictions.eq(RequestEntity.ATTR_clientId, clientId));
        }

        if (remoteAddr != null) {
            criteria.add(Restrictions.eq(RequestEntity.ATTR_ip, remoteAddr));
        }

        criteria.addOrder(Order.desc(RequestEntity.ATTR_attemptDate));

        List<RequestEntity> requestList = criteria.list();

        if (initLazy) {

            for (RequestEntity requestEntity : requestList) {
                requestEntity.getClient().getId();

                if (requestEntity.getLoan() != null) {
                    requestEntity.getLoan().getId();
                }
            }
        }

        session.close();

        return requestList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ClientEntity getClientByName(final String name, final String surname) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(ClientEntity.class);
        criteria.add(Restrictions.eq(ClientEntity.ATTR_name, name));
        criteria.add(Restrictions.eq(ClientEntity.ATTR_surname, surname));

        List<ClientEntity> list = criteria.list();
        session.close();

        if (CollectionUtils.isEmpty(list)) {
            return null;
        } else {
            return list.get(0);
        }
    }

    @SuppressWarnings("rawtypes")
    public Object saveEntity(final Object obj, Class cl) {
        Session session = sessionFactory.openSession();
        session.save(cl.getName(), obj);
        session.close();

        return obj;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<LoanTypeEntity> getLoanTypeList(int term, double amount, final Date now) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(LoanTypeEntity.class);

        Disjunction amountDmin = Restrictions.disjunction();
        amountDmin.add(Restrictions.le(LoanTypeEntity.ATTR_minamount, amount));
        amountDmin.add(Restrictions.isNull(LoanTypeEntity.ATTR_minamount));
        criteria.add(amountDmin);

        Disjunction amountDmax = Restrictions.disjunction();
        amountDmax.add(Restrictions.ge(LoanTypeEntity.ATTR_maxamount, amount));
        amountDmax.add(Restrictions.isNull(LoanTypeEntity.ATTR_maxamount));
        criteria.add(amountDmax);

        Disjunction termDmin = Restrictions.disjunction();
        termDmin.add(Restrictions.le(LoanTypeEntity.ATTR_minterm, term));
        termDmin.add(Restrictions.isNull(LoanTypeEntity.ATTR_minterm));
        criteria.add(termDmin);

        Disjunction termDmax = Restrictions.disjunction();
        termDmax.add(Restrictions.ge(LoanTypeEntity.ATTR_maxterm, term));
        termDmax.add(Restrictions.isNull(LoanTypeEntity.ATTR_maxterm));
        criteria.add(termDmax);

        criteria.add(Restrictions.ge(LoanTypeEntity.ATTR_expireDate, now));
        criteria.add(Restrictions.eq(LoanTypeEntity.ATTR_status, Status.ACTIVE));

        criteria.addOrder(Order.desc(LoanTypeEntity.ATTR_percent));

        List<LoanTypeEntity> list = criteria.list();
        session.close();

        return list;
    }

    @SuppressWarnings("unchecked")
    public List<RequestEntity> getIpRequest(String ip, Date now) {

        Session session = sessionFactory.openSession();
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        c.add(Calendar.HOUR_OF_DAY, -24);

        Criteria criteria = session.createCriteria(RequestEntity.class);
        criteria.add(Restrictions.ge(RequestEntity.ATTR_attemptDate, c.getTime()));
        criteria.add(Restrictions.le(RequestEntity.ATTR_ip, ip));

        List<RequestEntity> list = criteria.list();
        session.close();

        return list;

    }

} // end class LoanDaoImpl
