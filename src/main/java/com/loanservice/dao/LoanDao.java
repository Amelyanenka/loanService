package com.loanservice.dao;

import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.LoanTypeEntity;
import com.loanservice.domain.RequestEntity;


public interface LoanDao {

    String BEAN_NAME = "dataDao";

    @Transactional(rollbackFor = Throwable.class, readOnly = true)
    List<RequestEntity> getRequestList(final Integer clientId, final String remoteAddr, boolean initLazy);

    @Transactional(rollbackFor = Throwable.class, readOnly = true)
    ClientEntity getClientByName(final String name, final String surname);

    @SuppressWarnings("rawtypes")
    @Transactional(rollbackFor = Throwable.class, readOnly = false)
    Object saveEntity(final Object obj, Class cl);

    @Transactional(rollbackFor = Throwable.class, readOnly = true)
    List<LoanTypeEntity> getLoanTypeList(int term, double amount, final Date now);

    @Transactional(rollbackFor = Throwable.class, readOnly = true)
    List<RequestEntity> getIpRequest(String ip, Date now);

}
