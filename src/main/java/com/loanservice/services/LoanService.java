package com.loanservice.services;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.RequestEntity;


public interface LoanService {

    String BEAN_NAME = "loanService";

    List<RequestEntity> getRequestList(final Integer clientId, final String remoteAddr, boolean initLazy);

    @Transactional(rollbackFor = Throwable.class, readOnly = false)
    ClientEntity getOrCreateClient(final String clientName, final String clientSurname);

    @Transactional(rollbackFor = Throwable.class, readOnly = false)
    RequestEntity createLoan(int term, double amount, int client, String ip);

}
