package com.loanservice.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.loanservice.controller.ApplicationConfig;
import com.loanservice.dao.LoanDao;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.ErrorCode;
import com.loanservice.domain.LoanTypeEntity;
import com.loanservice.domain.RequestEntity;
import com.loanservice.domain.Status;


@Service(LoanService.BEAN_NAME)
public class LoanServiceImpl
    implements LoanService {

    static final Logger logger = Logger.getLogger(LoanServiceImpl.class);
    private static SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");

    private LoanDao dataDao;

    private ApplicationConfig config;

    @Override
    public List<RequestEntity> getRequestList(final Integer clientId, final String remoteAddr, boolean initLazy) {
        return dataDao.getRequestList(clientId, remoteAddr, initLazy);
    }

    @Override
    public ClientEntity getOrCreateClient(final String clientName, final String clientSurname) {
        ClientEntity clientByName = dataDao.getClientByName(clientName, clientSurname);

        if (clientByName == null) {
            clientByName = new ClientEntity();
            clientByName.setName(clientName);
            clientByName.setSurname(clientSurname);
            clientByName.setRegDate(new Date());
            clientByName.setStatus(Status.ACTIVE);
            dataDao.saveEntity(clientByName, ClientEntity.class);
        }

        return clientByName;
    }

    @Override
    public RequestEntity createLoan(int term, double amount, int clientId, String ip) {
        Date now = config.getNow();
        RequestEntity request = new RequestEntity(now, term, amount, clientId, ip);

        if (isRiskPeriod(now) && (amount == config.getMaxAmount())) {
            return saveRequest(request, ErrorCode.NIGHT_REQUEST_WITH_MAX_AMOUNT, null);
        }

        List<RequestEntity> requests = dataDao.getIpRequest(ip, now);

        if (requests.size() >= config.getMaxAttemptCount()) {
            return saveRequest(request, ErrorCode.OVER_COUNT, null);
        }

        List<LoanTypeEntity> loanTypeByRequest = dataDao.getLoanTypeList(term, amount, now);

        if (CollectionUtils.isEmpty(loanTypeByRequest)) {
            return saveRequest(request, ErrorCode.NO_LOANS, null);
        } else {

            return saveRequest(request, null, loanTypeByRequest.get(0));
        }

    }

    private RequestEntity saveRequest(RequestEntity entity, ErrorCode err, LoanTypeEntity loan) {
        entity.setErrorCode(err);

        if (loan != null) {
            entity.setLoanId(loan.getId());
        }

        dataDao.saveEntity(entity, RequestEntity.class);

        return entity;
    }

    private boolean isRiskPeriod(final Date now) {

        try {
            Calendar c = Calendar.getInstance();
            Date timeS = f.parse(config.getStartRiskPeriodString());
            c.setTime(timeS);

            Date startRiskPeriod = c.getTime();
            Date timeE = f.parse(config.getEndRiskPeriodString());
            c.setTime(timeE);

            Date endRiskPeriod = c.getTime();
            String timeString = f.format(now);
            Date time = f.parse(timeString);
            c.setTime(time);

            return time.before(endRiskPeriod) && time.after(startRiskPeriod);
        } catch (ParseException e) {
            logger.error("Time parse exception ", e);

            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setDataDao(@Qualifier(LoanDao.BEAN_NAME) final LoanDao dataDao) {
        this.dataDao = dataDao;
    }

    @Autowired
    public void setApplicationConfig(@Qualifier(ApplicationConfig.BEAN_NAME) final ApplicationConfig config) {
        this.config = config;
    }
} // end class LoanServiceImpl
