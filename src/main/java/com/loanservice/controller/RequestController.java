package com.loanservice.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.loanservice.domain.ClientEntity;
import com.loanservice.domain.RequestEntity;
import com.loanservice.services.LoanService;


@Controller
public class RequestController {

    static final Logger logger = Logger.getLogger(RequestController.class);

    @Autowired
    @Qualifier(LoanService.BEAN_NAME)
    LoanService dataService;

    @RequestMapping("form")
    public ModelAndView getForm(@ModelAttribute
        Request request) {
        return new ModelAndView("form");
    }

    @RequestMapping("register")
    public ModelAndView registerRequest(@ModelAttribute @Valid
        Request request, BindingResult result, HttpServletRequest req) {
        ModelAndView errorResult = new ModelAndView("form");

        if (result.hasErrors()) {
            return errorResult;
        }

        ClientEntity c = dataService.getOrCreateClient(request.getClientName(), request.getClientSurname());

        RequestEntity requestEntity = dataService.createLoan(request.getTerm(), request.getAmount(), c.getId(),
                req.getRemoteAddr());

        if (requestEntity.getErrorCode() != null) {
            errorResult.addObject("exception", requestEntity.getMessage());

            return errorResult;
        }

        ModelAndView modelAndView = new ModelAndView("redirect:list");
        modelAndView.addObject("id", c.getId());

        return modelAndView;
    }

    @RequestMapping("list")
    public ModelAndView getList(@RequestParam(required = true) int id) {
        @SuppressWarnings("rawtypes")
        List requestList = dataService.getRequestList(id, null, true);

        return new ModelAndView("list", "requestList", requestList);
    }

    @RequestMapping("listip")
    public ModelAndView getList(HttpServletRequest req) {
        @SuppressWarnings("rawtypes")
        List requestList = dataService.getRequestList(null, req.getRemoteAddr(), true);

        return new ModelAndView("list", "requestList", requestList);
    }

} // end class RequestController
