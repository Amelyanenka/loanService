package com.loanservice.controller;

import java.util.Date;
import javax.inject.Inject;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


@Configuration(ApplicationConfig.BEAN_NAME)
@PropertySource("classpath:app.properties")
public class ApplicationConfigImpl
    implements ApplicationConfig {

    @Inject
    Environment env;

    public int getMaxAttemptCount() {
        return new Integer(env.getProperty("max_attempt_count"));
    }

    public double getMaxAmount() {
        return new Double(env.getProperty("max_amount"));
    }

    public String getStartRiskPeriodString() {
        return env.getProperty("startRiskPeriodString");
    }

    public String getEndRiskPeriodString() {
        return env.getProperty("endRiskPeriodString");
    }

    public Date getNow() {
        return new Date();
    }
}
