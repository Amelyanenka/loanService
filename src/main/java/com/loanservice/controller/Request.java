package com.loanservice.controller;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;


public class Request {

    @NotNull(message = "Please enter term.")
    @Max(value = 60, message = "Term can be no more than 60 months.")
    private Integer term;

    @NotNull(message = "Please enter your amount.")
    @Max(value = 50000, message = "Amount can be no more than 50000.")
    private Double amount;

    @NotEmpty(message = "Please enter your name.")
    private transient String clientName;

    @NotEmpty(message = "Please enter your surname.")
    private transient String clientSurname;

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientSurname() {
        return clientSurname;
    }

    public void setClientSurname(String clientSurname) {
        this.clientSurname = clientSurname;
    }

} // end class Request
