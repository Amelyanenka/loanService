package com.loanservice.controller;

import java.util.Date;


public interface ApplicationConfig {
    String BEAN_NAME = "Config";

    int getMaxAttemptCount();

    double getMaxAmount();

    String getStartRiskPeriodString();

    String getEndRiskPeriodString();

    Date getNow();
}
