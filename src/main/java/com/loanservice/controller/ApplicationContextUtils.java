package com.loanservice.controller;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


@Component
public class ApplicationContextUtils
    implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
        throws BeansException {
        ApplicationContextUtils.context = applicationContext;

    }

    public static ApplicationContext getApplicationContext() {

        return ApplicationContextUtils.context;
    }

} // end class ApplicationContextUtils
