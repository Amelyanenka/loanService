package com.loanservice.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "REQUEST")
public class RequestEntity
    extends AbstractAutoIdEntity {

    public static final long serialVersionUID = 6402521929217517161L;
    public static final String ATTR_clientId = "clientId";
    public static final String ATTR_ip = "ip";
    public static final String ATTR_attemptDate = "attemptDate";

    @Column(name = "ATTEMPT_DATE")
    private Date attemptDate;

    @Column(name = "TERM")
    private Integer term;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "CLIENT_ID")
    private Integer clientId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENT_ID", insertable = false, updatable = false)
    private ClientEntity client;

    @Column(name = "LOAN_TYPE_ID")
    private Integer loanId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOAN_TYPE_ID", insertable = false, updatable = false)
    private LoanTypeEntity loan;

    @Column(name = "MESSAGE")
    @Enumerated
    private ErrorCode errorCode;

    @Column(name = "IP")
    private String ip;

    public RequestEntity() { }

    public RequestEntity(Date attemptDate, Integer term, Double amount, Integer clientId, String ip) {
        this.attemptDate = attemptDate;
        this.term = term;
        this.amount = amount;
        this.clientId = clientId;
        this.ip = ip;

    }

    public Date getAttemptDate() {
        return attemptDate;
    }

    public void setAttemptDate(Date attemptDate) {
        this.attemptDate = attemptDate;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public LoanTypeEntity getLoan() {
        return loan;
    }

    public void setLoan(LoanTypeEntity loan) {
        this.loan = loan;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return (errorCode != null) ? errorCode.getMessage() : "";
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }
} // end class RequestEntity
