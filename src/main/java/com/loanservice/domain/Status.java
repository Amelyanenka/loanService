package com.loanservice.domain;

public enum Status {
    INACTIVE,
    ACTIVE
}
