package com.loanservice.domain;

public enum ErrorCode {
    NIGHT_REQUEST_WITH_MAX_AMOUNT("Night attept with maximum amount"),
    OVER_COUNT("It has reached the maximum number of attempts"),
    NO_LOANS("No offers matching");

    private String message;

    private ErrorCode(String message) {
        this.message = message;

    }

    public String getMessage() {
        return message;
    }

}
