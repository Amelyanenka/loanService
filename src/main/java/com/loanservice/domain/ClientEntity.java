package com.loanservice.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;


@Entity
@Table(name = "CLIENT")
public class ClientEntity
    extends AbstractAutoIdEntity {

    private static final long serialVersionUID = 6811062322963947777L;

    public static final String ATTR_name = "name";
    public static final String ATTR_surname = "surname";

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "REG_DATE")
    private Date regDate;

    @Enumerated
    @Column(name = "STATUS")
    private Status status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

} // end class ClientEntity
