package com.loanservice.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;


@Entity
@Table(name = "LOAN_TYPE")
public class LoanTypeEntity
    extends AbstractAutoIdEntity {

    private static final long serialVersionUID = -3620787349048486318L;
    public static final String ATTR_maxterm = "maxTerm";
    public static final String ATTR_minterm = "minTerm";
    public static final String ATTR_expireDate = "expireDate";
    public static final String ATTR_status = "status";
    public static final String ATTR_maxamount = "maxAmount";
    public static final String ATTR_minamount = "minAmount";
    public static final String ATTR_percent = "percent";

    @Column(name = "NAME")
    private String name;

    @Column(name = "MAX_TERM")
    private Integer maxTerm;

    @Column(name = "MIN_TERM")
    private Integer minTerm;

    @Column(name = "PERCENT")
    private Double percent;

    @Column(name = "EXPIRE_DATE")
    private Date expireDate;

    @Enumerated
    @Column(name = "STATUS")
    private Status status;

    @Column(name = "MAX_AMOUNT")
    private Double maxAmount;

    @Column(name = "MIN_AMOUNT")
    private Double minAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxTerm() {
        return maxTerm;
    }

    public void setMaxTerm(Integer maxTerm) {
        this.maxTerm = maxTerm;
    }

    public Integer getMinTerm() {
        return minTerm;
    }

    public void setMinTerm(Integer minTerm) {
        this.minTerm = minTerm;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("Name=" + this.getName());
        buffer.append(";");
        buffer.append("percent=" + this.getPercent());
        buffer.append("%.");

        return buffer.toString();
    }

    public String getString() {
        return toString();
    }
} // end class LoanTypeEntity
