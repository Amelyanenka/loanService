package com.loanservice.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class AbstractAutoIdEntity
    implements Serializable {

    private static final long serialVersionUID = -7916599469252273690L;

    @Id
    @Column(unique = true, nullable = false, insertable = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
