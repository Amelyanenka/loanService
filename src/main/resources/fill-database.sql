INSERT INTO LOAN_TYPE (NAME, MIN_TERM, MAX_TERM, PERCENT, EXPIRE_DATE, STATUS, MIN_AMOUNT, MAX_AMOUNT) VALUES ('Loan30%', 1, 3, 30.5, '2016-10-01', 1,30000, 50000);
INSERT INTO LOAN_TYPE (NAME, MAX_TERM,  PERCENT, EXPIRE_DATE, STATUS,MAX_AMOUNT) VALUES ('Loan20%', 60,  20, '2016-12-01', 1, 10000);
INSERT INTO LOAN_TYPE (NAME, MIN_TERM, PERCENT, EXPIRE_DATE, STATUS, MIN_AMOUNT) VALUES ('Loan10%', 50, 10, '2016-11-01', 1, 20000);
