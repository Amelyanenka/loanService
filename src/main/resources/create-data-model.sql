CREATE TABLE CLIENT (
   ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
   NAME VARCHAR(45) NOT NULL,
   SURNAME VARCHAR(45) NOT NULL,
   REG_DATE DATE NOT NULL,
   STATUS TINYINT NOT NULL,
  PRIMARY KEY (ID));

CREATE TABLE LOAN_TYPE (
   ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
   NAME varchar(45) NOT NULL,
   MIN_TERM TINYINT,
   MAX_TERM TINYINT,
   PERCENT decimal(4,2) NOT NULL,
   EXPIRE_DATE date NOT NULL,
   STATUS TINYINT NOT NULL,
   MIN_AMOUNT decimal(11,2),
   MAX_AMOUNT decimal(11,2),
  PRIMARY KEY (ID));

CREATE TABLE REQUEST (
   ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
   ATTEMPT_DATE timestamp NOT NULL,
   TERM TINYINT,
   AMOUNT decimal(11,2),
   CLIENT_ID INT NOT NULL,
   LOAN_TYPE_ID INT,
   MESSAGE TINYINT,
   IP varchar(20) DEFAULT NULL,
  PRIMARY KEY (ID),
  FOREIGN KEY (CLIENT_ID) REFERENCES CLIENT (ID),
  FOREIGN KEY (LOAN_TYPE_ID) REFERENCES LOAN_TYPE (ID));