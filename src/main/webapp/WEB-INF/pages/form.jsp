<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Request Form</title>
<style>
  <%@include file='../css/main.css'%>
</style>
</head>
<body>
  <center>
    <form:form id="registerForm" modelAttribute="request" method="post" action="register">
      <table class="requestTable">
        <tr>
          <td />
          <td style="text-align: center;"><div class="title">New request</div></td>
        </tr>
        <tr>
          <td><form:label path="clientName" class="label">Name</form:label></td>
          <td><form:input path="clientName" value="${requestObject.clientName}" /></td>
        </tr>
        <tr>
          <td />
          <td><form:errors path="clientName" class="error" /></td>
        </tr>
        <tr>
          <td><form:label path="clientSurname" class="label">Surname</form:label></td>
          <td><form:input path="clientSurname" value="${requestObject.clientSurname}" /></td>
        </tr>
        <tr>
          <td />
          <td><form:errors path="clientSurname" class="error" /></td>
        </tr>        
        <tr>
          <td><form:label path="term" class="label">Number of months</form:label></td>
          <td><form:input path="term" value="${requestObject.term}" /></td>
        </tr>
        <tr>
          <td />
          <td><form:errors path="term" class="error" /></td>
        </tr>
        <tr>
          <td><form:label path="amount" class="label">Amount</form:label></td>
          <td><form:input path="amount" value="${requestObject.amount}" /></td>
        </tr>
        <tr>
          <td />
          <td><form:errors path="amount" class="error" /></td>
        </tr>
        <tr>
          <td></td>
          <td style="text-align: center;">
            <input class="submit" type="submit" value="Send request" width="50px;" />
          </td>
        </tr>
        <tr>
          <td></td>
          <td style="text-align: center;">
            <input class="submit" onClick="parent.location='listip'" type="button" value="This IP requests" width="200px;" />
          </td>
        </tr>        
        <tr>
          <td />
          <td style="text-align: center;"><div class="title">Operation response</div></td>
        </tr>
        <tr>
          <td />
          <td><textarea readonly rows="5" cols="10">${exception}</textarea></td>
        </tr>
      </table>
    </form:form>
  </center>
</body>
</html>
