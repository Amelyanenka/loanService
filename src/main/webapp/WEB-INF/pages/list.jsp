<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Request list</title>
<style>
  <%@include file='../css/main.css'%>
</style>
</head>
<body>
  <center>
    <div class="title">Request list</div>
    <c:if test="${!empty requestList}">
      <table width="600px">
        <tr>
          <th>Date</th>
          <th>Client</th>
          <th>Term</th>
          <th>Amount</th>
          <th>Loan Type</th>
          <th>Message</th>
        </tr>
        <c:forEach items="${requestList}" var="request">
          <tr>
            <td><fmt:formatDate value="${request.attemptDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td><c:out value="${request.client.name} ${request.client.surname}" /></td>
            <td><c:out value="${request.term}" /></td>
            <td><c:out value="${request.amount}" /></td>
            <td><c:out value="${request.loan.string}" /></td>
            <td><c:out value="${request.message}" /></td>
          </tr>
        </c:forEach>
      </table>
    </c:if>
    <input class="submit" onClick="parent.location='form'" type="button" value="Click Here to add new Request" width="200px;" />
  </center>
</body>
</html>